## DCacheDisable
Type: linux kernel module

OS: ubuntu 16.04

Platform: Gluon boards + ZynqUltrascale+ XCZU9EG

Cpu: ARM A53 on ZU7

Usage: insmod = disable DCache while rmmod = enable DCache

Detail: 

My platform is cortex-a53, ubuntu 16.04 started and supported SMP on four cpu cores. Thus, I need to turn off multi-core to ensure L2-cache coherency. Thanks to the feature of cpu-hot-plug, I just execute :
```
echo '0' > /sys/devices/system/cpu/cpu1/online,
echo '0' > /sys/devices/system/cpu/cpu2/online,
echo '0' > /sys/devices/system/cpu/cpu3/online
```
then I execute `dmesg` to verify that multi-core has been turn off.


NOTE :  EL2 should switch to EL1
In summary: I build the linux module. With GUN inline asm , I flush all cache and set sctlr_el1.c 0.
