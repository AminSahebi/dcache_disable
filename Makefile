ifneq ($(KERNELRELEASE),)
        obj-m := DCacheDisable.o
else
        BUILDROOT := ${HOME}/linaro/gcc-linaro-5.3.1-2016.05-x86_64_aarch64-linux-gnu
        KERN_DIR := ${HOME}/Desktop/Doc/TEBF0808-default/os/Gluon/build/linux/kernel/xlnx-4.6
        PWD := $(shell pwd)
default:
	$(MAKE) ARCH=arm64 CROSS_COMPILE=${BUILDROOT}/bin/aarch64-linux-gnu- -C $(KERN_DIR) M=$(PWD) modules
endif

clean:
	rm -rf *.o *~ core .depend .*.cmd *.ko *.mod.c .tmp_versions

